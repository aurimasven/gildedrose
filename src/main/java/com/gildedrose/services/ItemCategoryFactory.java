package com.gildedrose.services;

import com.gildedrose.model.Categories;
import com.gildedrose.model.ItemWrapper;
import com.gildedrose.services.categories.*;

/*
Item category factory
Returns an instance of an item based on its' category.
 */
public class ItemCategoryFactory {
    static ItemCategory getItemCategory(ItemWrapper item) {
        if (item.getCategory().equals(Categories.LEGENDARY))
            return new Legendary(item);
        else if (item.getCategory().equals(Categories.CHEESE))
            return new Cheese(item);
        else if (item.getCategory().equals(Categories.PASS))
            return new BackstagePass(item);
        else if (item.getCategory().equals(Categories.CONJURED))
            return new Conjured(item);
        return new Regular(item) {
        };
    }
}
