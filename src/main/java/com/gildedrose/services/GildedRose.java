package com.gildedrose.services;

import com.gildedrose.model.Item;
import com.gildedrose.model.ItemWrapper;
import com.gildedrose.respository.InventoryItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GildedRose {

    private List<ItemWrapper> items;

    public void updateQuality() {
        items.forEach(c -> ItemCategoryFactory.getItemCategory(c).updateItem());
    }

    @Autowired
    public void setItems(InventoryItemRepo invItems) {
        if (items == null)
            items = invItems.getItems();
    }

    public List<ItemWrapper> getItems() {
        return this.items;
    }

    /*
    For easy backwards compatibility with first golden master test
    Not needed after new tests are created
     */
    private List<Item> toItemList(List<ItemWrapper> items) {
        return items.stream().map(m -> m.getItem()).collect(Collectors.toList());
    }

}