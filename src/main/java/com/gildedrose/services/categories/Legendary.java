package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;

/*
Legendary items do not degrade or change any value.
Class overrides any default logic.
 */
public class Legendary extends ItemCategory {

    public Legendary(ItemWrapper item) {
        super(item);
    }

    @Override
    protected void checkExpiration(ItemWrapper item) {

    }

    @Override
    protected void checkSellIn(ItemWrapper item) {

    }

    @Override
    protected void checkQuality(ItemWrapper item) {

    }
}
