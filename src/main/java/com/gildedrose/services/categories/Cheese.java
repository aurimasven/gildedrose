package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;

/*
Cheese increases in quality the older it gets
 */
public class Cheese extends ItemCategory {

    public Cheese(ItemWrapper item) {
        super(item);
    }

    @Override
    protected void checkExpiration(ItemWrapper item) {
        incrementQuality(item);
    }

    @Override
    protected void checkQuality(ItemWrapper item) {
        incrementQuality(item);
    }
}
