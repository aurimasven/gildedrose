package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;

/*
Regular items have default behaviour
 */
public class Regular extends ItemCategory {
    public Regular(ItemWrapper item) {
        super(item);
    }
}
