package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;
/*
"Conjured" items degrade in Quality twice as fast as normal items
 */
public class Conjured extends ItemCategory {
    public Conjured(ItemWrapper item) {
        super(item);
    }

    @Override
    protected void checkExpiration(ItemWrapper item) {
        decrementQuality(item);
        decrementQuality(item);
    }

    @Override
    protected void checkQuality(ItemWrapper item) {
        decrementQuality(item);
        decrementQuality(item);
    }
}
