package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;

/*
Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
    Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
	Quality drops to 0 after the concert
 */
public class BackstagePass extends ItemCategory {
    public BackstagePass(ItemWrapper item) {
        super(item);
    }

    @Override
    protected void checkExpiration(ItemWrapper item) {
        item.setQuality(0);
    }

    @Override
    protected void checkQuality(ItemWrapper item) {
        incrementQuality(item);
        if (item.getSellIn() < 11)
            incrementQuality(item);

        if (item.getSellIn() < 6)
            incrementQuality(item);
    }
}
