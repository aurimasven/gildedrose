package com.gildedrose.services.categories;

import com.gildedrose.model.ItemWrapper;

/*
Main Item quality handling class. Override methods in updateItem() to change regular behaviour.
Leaving this class abstract in case more abstraction is needed.
 */
public abstract class ItemCategory {

    ItemWrapper item;

    private static final int MAXIMUM_QUALITY = 50;

    public ItemCategory(ItemWrapper item) {
        this.item = item;
    }

    //Separate core logic
    public void updateItem() {
        checkQuality(item);
        checkSellIn(item);
        if (needsExpirationCheck())
            checkExpiration(item);
    }

    protected void checkExpiration(ItemWrapper item) {
        decrementQuality(item);
    }
    protected void checkSellIn(ItemWrapper item) {
        item.setSellIn(item.getSellIn() - 1);
    }
    protected void checkQuality(ItemWrapper item) {
        decrementQuality(item);
    }

    private boolean needsExpirationCheck() {
        if (item.getSellIn() < 0)
            return true;
        return false;
    }

    protected void incrementQuality(ItemWrapper item) {
        if (item.getQuality() < MAXIMUM_QUALITY)
            item.setQuality(item.getQuality() + 1);
    }

    protected void decrementQuality(ItemWrapper item) {
        if (item.getQuality() > 0)
            item.setQuality(item.getQuality() - 1);
    }
}
