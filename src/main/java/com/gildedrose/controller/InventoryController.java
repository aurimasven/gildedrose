package com.gildedrose.controller;

import com.gildedrose.model.ItemWrapper;
import com.gildedrose.services.GildedRose;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InventoryController {

    @Autowired
    GildedRose gildedRose;

    @RequestMapping("/getItemList")
    public List<ItemWrapper> getItemList() {
        return gildedRose.getItems();
    }
}
