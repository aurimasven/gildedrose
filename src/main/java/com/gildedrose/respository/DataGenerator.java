package com.gildedrose.respository;

import com.gildedrose.model.Categories;
import com.gildedrose.model.Item;
import com.gildedrose.model.ItemWrapper;

import java.util.Arrays;
import java.util.List;

/*
As no DB is present, need to fake data
 */
public class DataGenerator {
    public static List<ItemWrapper> generateData() {
        return Arrays.asList(new ItemWrapper[]{
                new ItemWrapper(new Item("+5 Dexterity Vest", 10, 20), Categories.REGULAR),
                new ItemWrapper(new Item("Aged Brie", 2, 0), Categories.CHEESE),
                new ItemWrapper(new Item("Aged Brie", 2, 10), Categories.CHEESE),
                new ItemWrapper(new Item("Elixir of the Mongoose", 5, 7), Categories.REGULAR),
                new ItemWrapper(new Item("Elixir of the Mongoose", 4, 8), Categories.REGULAR),
                new ItemWrapper(new Item("Sulfuras, Hand of Ragnaros", 0, 80), Categories.LEGENDARY),
                new ItemWrapper(new Item("Sulfuras, Hand of Ragnaros", -1, 70), Categories.LEGENDARY),
                new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20), Categories.PASS),
                new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49), Categories.PASS),
                new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49), Categories.PASS),
                // this conjured item does not work properly yet
                new ItemWrapper(new Item("Conjured Mana Cake", 3, 6), Categories.CONJURED),
                new ItemWrapper(new Item("Conjured Mana Cake", 7, 5), Categories.CONJURED),
                new ItemWrapper(new Item("Conjured Mana Cake", 7, 5), Categories.CONJURED)
        });
    }
}
