package com.gildedrose.respository;

import com.gildedrose.model.ItemWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class InventoryItemRepo {

    List<ItemWrapper> items  = DataGenerator.generateData();
    @Autowired
    public List<ItemWrapper> getItems() {
        return items;
    }

    // Mainly for testing purposes, returns self instance
    public InventoryItemRepo setItems(List<ItemWrapper> items) {
        this.items = items;
        return this;
    }
}
