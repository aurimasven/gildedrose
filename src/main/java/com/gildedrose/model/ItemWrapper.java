package com.gildedrose.model;

public class ItemWrapper {
    private Item item;
    private Categories category;

    public ItemWrapper(Item item, Categories category) {
        this.item = item;
        this.category = category;
    }

    public Item getItem() {
        return this.item;
    }

    public Categories getCategory() {
        return this.category;
    }

    public String getName() {
        return this.item.name;
    }

    public int getSellIn() {
        return this.item.sellIn;
    }

    public int getQuality() {
        return this.item.quality;
    }

    public void setName(String name) {
        this.item.name = name;
    }

    public void setSellIn(int sellIn) {
        this.item.sellIn = sellIn;
    }

    public void setQuality(int quality) {
        this.item.quality = quality;
    }

    @Override
    public String toString() {
        return this.getCategory().name + ", " + this.getName() + ", " + this.getSellIn() + ", " + this.getQuality();
    }
}
