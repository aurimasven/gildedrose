package com.gildedrose.model;

/*
Category enum.
As there's no DB, storing categories here
 */
public enum Categories {
    REGULAR("Regular"),
    CHEESE("Cheese"),
    LEGENDARY("Legendary"),
    PASS("Backstage pass"),
    CONJURED("Conjured");

    String name;

    private Categories(String name) {
        this.name = name;
    }
}
