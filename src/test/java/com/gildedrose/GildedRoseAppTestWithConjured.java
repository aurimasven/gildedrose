package com.gildedrose;

import com.gildedrose.model.Categories;
import com.gildedrose.model.Item;
import com.gildedrose.model.ItemWrapper;
import com.gildedrose.respository.InventoryItemRepo;
import com.gildedrose.services.GildedRose;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GildedRoseAppTestWithConjured {

    @Autowired
    private MockMvc mvc;

    // Days passed in Gilded Rose
    private static final int DAYS = 30;
    // GoldenMaster file destination
    private static final String GOLDEN_MASTER_FILE = "resources/GoldenMasterConjured.txt";

    // Items to test with
    List<ItemWrapper> items = Arrays.asList(new ItemWrapper[]{
            new ItemWrapper(new Item("+5 Dexterity Vest", 10, 20), Categories.REGULAR),
            new ItemWrapper(new Item("Aged Brie", 2, 0), Categories.CHEESE),
            new ItemWrapper(new Item("Aged Brie", 2, 10), Categories.CHEESE),
            new ItemWrapper(new Item("Elixir of the Mongoose", 5, 7), Categories.REGULAR),
            new ItemWrapper(new Item("Elixir of the Mongoose", 4, 8), Categories.REGULAR),
            new ItemWrapper(new Item("Sulfuras, Hand of Ragnaros", 0, 80), Categories.LEGENDARY),
            new ItemWrapper(new Item("Sulfuras, Hand of Ragnaros", -1, 70), Categories.LEGENDARY),
            new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20), Categories.PASS),
            new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49), Categories.PASS),
            new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49), Categories.PASS),
            // this conjured item does not work properly yet
            new ItemWrapper(new Item("Conjured Mana Cake", 3, 6), Categories.CONJURED),
            new ItemWrapper(new Item("Conjured Mana Cake", 7, 5), Categories.CONJURED)
    });


    @Test
    public void testAgainstGoldenMaster() {
        // Get output after traversing days
        String testString = traverseDays(DAYS);
        Path goldenMasterPath = Paths.get(GOLDEN_MASTER_FILE);

        if (!Files.exists(goldenMasterPath))
            createGoldenMaster(goldenMasterPath, testString);
        else
            compareToGoldenMaster(goldenMasterPath, testString);
    }

    @Test
    public void getItemList() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getItemList").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(notNullValue()));
    }

    private void compareToGoldenMaster(Path goldenMasterPath, String log) {
        StringBuilder sb = new StringBuilder();
        try (Stream<String> stream = Files.lines(goldenMasterPath)) {
            stream.forEach(s -> sb.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(log, sb.toString());
    }

    private void createGoldenMaster(Path goldenMasterPath, String log) {
        try {
            System.out.println("Creating Golden Master file.");
            if (!Files.exists(goldenMasterPath.getParent().toAbsolutePath())) {
                System.out.println("Creating directory structure for Golden Master.");
                Files.createDirectories(goldenMasterPath.getParent().toAbsolutePath());
                System.out.println("Directory created successfully");
            }
            if (goldenMasterPath.toFile().createNewFile()) {
                Files.write(goldenMasterPath, log.getBytes());
                System.out.println("Golden master file created at " + goldenMasterPath.toAbsolutePath());
            }
        } catch (IOException e) {
            System.out.println("Failed to create Golden Master file!!!");
            e.printStackTrace();
        }

    }

    private String traverseDays(int days) {
        GildedRose gildedRose = new GildedRose();
        gildedRose.setItems(new InventoryItemRepo().setItems(items));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < days; i++) {
            sb.append("-------- day " + i + " --------\n");
            sb.append("name, sellIn, quality\n");
            for (ItemWrapper item : gildedRose.getItems()) {
                sb.append(item).append("\n");
            }
            sb.append("\n");
            gildedRose.updateQuality();
        }

        gildedRose.updateQuality();
        sb.append(getStringRepresentation(gildedRose.getItems()));
        return sb.toString();
    }
    private String getStringRepresentation(List<ItemWrapper> items) {
        StringBuilder builder = new StringBuilder();
        items.forEach(c -> builder.append(c).append("\n"));
        return builder.toString();
    }
}
